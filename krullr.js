#!/usr/bin/env node
const axios = require('axios');
const cheerio = require('cheerio');
const robo = require('robots-parser');
const {URL} = require('url');

class Krullr {
  getPage = async (url) => {
    return await axios.get(url);
  };
  loadPage = async (url) => {
    const {data} = await this.getPage(url);
    return cheerio.load(data);
  };
  pages = {};
  crawl = async (url) => {
    const path = new URL(url).pathname;
    if(this.pages[path] || !this.robo.isAllowed(this.base, url)) { return; }
    this.pages[path] = { outlinks: {}, inlinks: {} } ;
    let $;
    try { $ = await this.loadPage(url) } catch { return; };
    const links = $("a");
    const tasks = [];
    links.each((i, e) => {
      const h = $(e).attr('href');
      if(!h) { return; }
      const href = h.replace(/^\/\//,'https://');
      let subUrl;
      if(!href.match(new RegExp(this.base))) {
        try {
          new URL(href);
        } catch (e) {
          subUrl = new URL(href, this.base);
        }
      } else {
        subUrl = new URL(href);
      }
      if(subUrl == null) { return; }
      const sp = subUrl.pathname;
      this.pages[path].outlinks[sp] = true;
      tasks.push(this.crawl(subUrl.href));
    })
    await Promise.all(tasks);
    Object.keys(this.pages).forEach(url => {
      Object.keys(this.pages[url].outlinks).forEach(sp => {
        if(sp === url) { return; }
        //console.log('adding inlink from',url,'to',sp);
        this.pages[sp].inlinks[url] = true;
      })
    });
    return this.pages;
    }
  constructor(baseUrl) {
    baseUrl = baseUrl.replace(/\/$/,'');
    const url = new URL(baseUrl);
    this.base = baseUrl;
    this.host = url.hostname;
  }
  async run() {
    this.robo = robo(this.base, (await axios.get(`${this.base}/robots.txt`)).data);
    return (await this.crawl(this.base));
  }


}

(async() => {
  const url = process.argv.slice(2).shift();
  if(!url) {
    console.error("Pls provide url pls ty");
    return;
  }
  console.log("crawling",url);
  const k = new Krullr(url);
  const f = await k.run();
  console.log('im poopin');
  console.dir(f);
})();
